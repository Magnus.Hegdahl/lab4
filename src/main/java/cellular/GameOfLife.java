package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    /**
     * 
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     * 
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */
    public GameOfLife(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int column = 0; column < currentGeneration.numColumns(); ++column) {
                currentGeneration.set(row, column,
                    new CellState[]{CellState.ALIVE, CellState.DEAD}[random.nextInt(2)]
                );
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row = 0; row < numberOfRows(); ++row)
            for (int column = 0; column < numberOfRows(); ++column)
                nextGeneration.set(row, column, getNextCell(row, column));
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int column) {
        CellState current = getCellState(row, column);
        int minNeighbor, maxNeighbor;
        switch (current) {
            case ALIVE:
                minNeighbor = 2;
                maxNeighbor = 3;
                break;
            case DEAD:
                minNeighbor = 3;
                maxNeighbor = 3;
                break;
            default:
                throw new IllegalStateException(String.format(
                    "Unhandled state: %s", current
                ));
        }
        int aliveNeighbors = countNeighbors(row, column, CellState.ALIVE);

        if (minNeighbor <= aliveNeighbors && aliveNeighbors <= maxNeighbor)
            return CellState.ALIVE;
        else
            return CellState.DEAD;
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, column) on the board
     * 
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     * 
     * @param row     the row of the cell
     * @param column     the colun of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int column, CellState state) {
        int count = 0;
        for (int dRow = -1; dRow <= 1; ++dRow) {
            for (int dColumn = -1; dColumn <= 1; ++dColumn) {
                if (dRow == 0 && dColumn == 0) continue;
                int nRow = row + dRow;
                int nColumn = column + dColumn;
                if (0 > nRow || nRow >= numberOfRows()) continue;
                if (0 > nColumn || nColumn >= numberOfRows()) continue;
                if (getCellState(nRow, nColumn).equals(state))
                    ++count;
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
