package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    // must be stored because it can not be determined
    // from data[][] if the number of rows is zero.
    private final int columns;

    CellState data[][];

    public CellGrid(int rows, int columns, CellState initialState) {
        this.columns = columns;
        data = new CellState[rows][columns];
        for (CellState[] row : data)
            Arrays.fill(row, initialState);
    }

    @Override
    public int numRows() {
        return data.length;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        assertWithinBounds(row, column);
        data[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        assertWithinBounds(row, column);
        return data[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copy = new CellGrid(numRows(), numColumns(), null);
        for (int row = 0; row < numRows(); ++row)
            copy.data[row] = data[row].clone();
        return copy;
    }
    
    private void assertWithinBounds(int row, int column) {
        if (0 > row || row >= numRows())
            throw new IndexOutOfBoundsException(String.format(
                "The row %d is not in the range [0, %d)", row, numRows()
            ));
        if (0 > column || column >= numColumns())
            throw new IndexOutOfBoundsException(String.format(
                "The column %d is not in the range [0, %d)", row, numRows()
            ));
    }
}
